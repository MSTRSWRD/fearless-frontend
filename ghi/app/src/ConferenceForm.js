import React, { useEffect, useState } from 'react';


function ConferenceForm() {
    const [name, setName] = useState('');
    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const [starts, setStarts] = useState('');
    const handleStartChange = (e) => {
        const value = e.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');
    const handleEndChange = (e) => {
        const value = e.target.value;
        setEnds(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentations = (e) => {
        const value = e.target.value;
        setMaxPresentations(value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendees = (e) => {
        const value = e.target.value;
        setMaxAttendees(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);


        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }


    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" name="name"
                                className="form-control"/>
                                <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartChange} value={starts} placeholder="Starts" required type="date" id="starts" name="starts"
                                className="form-control"/>
                                <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndChange} value={ends} placeholder="Ends" required type="date" id="ends" name="ends"
                                className="form-control"/>
                                <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" name="description" rows="4"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentations} value={maxPresentations} placeholder="Maximum Presentations" required type="number" id="max_presentations"
                                name="max_presentations" className="form-control"/>
                                <label htmlFor="max_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendees} value={maxAttendees} placeholder="Maximum attendees" required type="number" id="max_attendees"
                                name="max_attendees" className="form-control"/>
                                <label htmlFor="max_attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
