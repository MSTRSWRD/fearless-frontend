function createCard(title, description, pictureUrl, starts, ends, location) {
    // if (response.ok) {
    return `
    <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${new Date(starts).toLocaleDateString()} -
            ${new Date(ends).toLocaleDateString()}
            </div>
        </div>
    `;
    // } else {
    // return `
    //     <div class="card" aria-hidden="true">
    //         <img src="${pictureUrl}" class="card-img-top">
    //         <div class="card-body">
    //             <h5 class="card-title placeholder-glow">
    //                 <span class="placeholder col-6"></span>
    //             </h5>
    //             <p class="card-text placeholder-glow">
    //                 <span class="placeholder col-7"></span>
    //                 <span class="placeholder col-4"></span>
    //                 <span class="placeholder col-4"></span>
    //                 <span class="placeholder col-6"></span>
    //                 <span class="placeholder col-8"></span>
    //             </p>
    //             <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
    //         </div>
    //     </div>
    // `;
}









function alert(wow) {
    return `
    <div class="alert alert-danger" role="alert">
    ${wow}
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const wow = "YO API AINT WORKIN"
            const notice = document.querySelector(`main`);
            notice.innerHTML = alert(wow);

        } else {
            const data = await response.json();

            let index = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector(`#col-${index % 3}`);
                    column.innerHTML += html;
                    index += 1;
                }
            }
        }
    } catch (e) {
        console.error(e);
        //console.error('error', error); //(attempt)figure out what to do if an error is raised
    }
});
